![banner.png](screens/banner.png?raw=true "banner.png")

A bot to play blackjack for the Internet Relay Chat (IRC) protocol.

##### Note
This script is not finished yet, just putting up my work thus far.

##### Configuration
Edit the `config.py` file for modify some of the bot settings.

The `mini_deck` setting will use a single line to display cards.

###### Commands
| Command | Description |
| --- | --- |
| @help | Information about the commands. |
| @cheat | Betting cheat sheet. |
| .hit | Draw a card. |
| .mini | Toggle the mini deck. |
| .play | Start a game. |
| .stand | Stop drawing cards. |
| .stop | End the curent game. |

##### Todo
- Add player database / chip system.
- Reward chips based on number of lines of chat in a channel. (Cap it to prevent flood.)
- Add a player versus player and a player versus computer system.
- Incorperate splits and double downs, etc.

##### Screens
![game.png](screens/game.png?raw=true "game.png")
![cheat.png](screens/cheat.png?raw=true "cheat.png")
